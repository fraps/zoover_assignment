﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using com.zoover.context;
using com.zoover.model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace com.zoover.dataservice.Controllers
{
    [Route("api/[controller]")]
    public class ReviewsController : Controller
    {
        private readonly Context _context;

        public ReviewsController(Context context)
        {
            _context = context;

        }

        // GET: api/<controller>
        [HttpGet]
        public async Task<IActionResult> GetAsync()
        {

            var result = await _context.Reviews
                                .Include(r => r.Cooperation)
                                    .ThenInclude(c => c.SyndicationPartner)
                                .Include(r => r.Status)
                                .Include(r => r.Texts)
                                .Include(r => r.Ratings)
                                    .ThenInclude(r => r.General)
                                .Include(r => r.Ratings)
                                    .ThenInclude(r => r.Aspects)
                                 .ToListAsync()
                                 ;//.AsEnumerable();

            if (null == result) return NotFound();

            return Ok(result);
        }

        // GET api/<controller>/00000000-0000-0000-0000-000000000000
        [HttpGet("{id}")]
        public async Task<IActionResult> GetAsync(Guid id)
        {
            var result = await _context.Reviews
                                .Include(r => r.Cooperation)
                                    .ThenInclude(c => c.SyndicationPartner)
                                .Include(r => r.Status)
                                .Include(r => r.Texts)
                                .Include(r => r.User)
                                .Include(r => r.Titles)
                                .Include(r => r.Ratings)
                                    .ThenInclude(r => r.General)
                                .Include(r => r.Ratings)
                                    .ThenInclude(r => r.Aspects)
                                .Include(r => r.Parents)
                                    .ThenInclude(p => p.Parent)
                                .FirstOrDefaultAsync(a => a.ID == id);
            if (result == null) return NotFound();

            return Ok(result);
        }

        
    }
}
