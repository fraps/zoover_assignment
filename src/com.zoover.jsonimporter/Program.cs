﻿using com.zoover.model;
using com.zoover.context;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using CommandLine;
using CommandLine.Text;
using System.Text;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;

namespace com.zoover.jsonimporter
{
    public class Importer<K> where K : class
    {
        private readonly string connectionString;
        
        public int Import(string fileName)
        {
            var contextOptions = new DbContextOptionsBuilder<Context>()
                                    .UseSqlServer(connectionString)
                                    .Options;

            using (var db = new Context(contextOptions))
            using (StreamReader r = new StreamReader(fileName))
            using (JsonReader reader = new JsonTextReader(r))
            {
                db.Database.EnsureCreated();
                JsonSerializer serializer = new JsonSerializer();
                serializer.PreserveReferencesHandling = PreserveReferencesHandling.All;

                var list = serializer.Deserialize<List<K>>(reader);
                db.Set<K>().AddRange(list);
                db.SaveChanges();
                return list.Count();
            }
        }

        public Importer(string connectionString)
        {
            this.connectionString = connectionString;
        }
    }



    class Program
    {
        class Options
        {
            [Option('f', "fileName", Required = true, HelpText = "File to be processed.")]
            public string FileName { get; set; }

            [Option('t', "Type", Required=true, HelpText = "Type of file to process ('Review'/'Accomodation')")]
            public string Type { get; set; }
            
        }



        static void Main(string[] args)
        {
           
            int itemsImported = 0;
            Parser.Default.ParseArguments<Options>(args)
                    .WithParsed<Options>(opts =>
                    {
                        try
                        {
                            var builder = new ConfigurationBuilder()
                               .SetBasePath(Directory.GetCurrentDirectory())
                               .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

                            IConfigurationRoot configuration = builder.Build();


                            if (opts.Type == "Review")
                            {
                                itemsImported = new Importer<Review>(configuration.GetConnectionString("ZooverDB")).Import(opts.FileName);
                            }
                            else if (opts.Type == "Accomodation")
                            {
                                itemsImported = new Importer<Accomodation>(configuration.GetConnectionString("ZooverDB")).Import(opts.FileName);
                            }
                            else
                            {
                                throw new ApplicationException($"Invalid option '{opts.Type}' for type of file to process.");
                            }
                        }
                        catch(Exception e)
                        {
                            Console.Error.WriteLine(e.Message);
                        }
                        
                    })
                    .WithNotParsed<Options>((errs) => Console.Error.WriteLine(errs));

            Console.WriteLine($"{itemsImported} items imported into database");
        }
    }
}
