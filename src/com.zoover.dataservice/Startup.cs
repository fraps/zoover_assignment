﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using com.zoover.context;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace com.zoover.dataservice
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IHostingEnvironment hostingEnvironment)
        {
            Configuration = configuration;
            HostingEnviroment = hostingEnvironment;
        }

        public IConfiguration Configuration { get; }
        public IHostingEnvironment HostingEnviroment { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            if (HostingEnviroment.IsEnvironment("Testing"))
            {
                //inject in-memory database to mock db context in testing
                services.AddDbContext<Context>(opts => opts.UseInMemoryDatabase("TestingDB")
                                                       .EnableSensitiveDataLogging(true)
                                                       .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking));
            }
            else
            {
                services.AddDbContext<Context>(opts => opts.UseSqlServer(Configuration.GetConnectionString("ZooverDB"))
                                                       .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking));
            }
            services.AddMvc();
        }


        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
        }
    }
}
