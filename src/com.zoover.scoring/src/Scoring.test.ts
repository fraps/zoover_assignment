import { expect } from 'chai';
import 'mocha';
import { Scoring } from './Scoring';
import { Review } from './models/Review';

const reviews : Review[] = [{
    "user": null,
    "titles":null,
    "texts": null,
    "travelDate": 1225135604000,
    "locale": "nl",
    "ratings": {
        "general": {
            "general": 9
        },
        "aspects": {
            "location": 9,
            "service": 9,
            "priceQuality": 9,
            "food": 6,
            "room": 7,
            "childFriendly": 10,
            "interior": 10,
            "size": null,
            "activities": null,
            "restaurants": null,
            "sanitaryState": null,
            "accessibility": null,
            "nightlife": null,
            "culture": null,
            "surrounding": null,
            "atmosphere": null,
            "noviceSkiArea": null,
            "advancedSkiArea": null,
            "apresSki": null,
            "beach": null,
            "entertainment": null,
            "environmental": null,
            "pool": null,
            "terrace": null,
            "housing": null,
            "hygiene": null
        }
    },
    "traveledWith": null,
    "cooperation": null,
    "entryDate": 1225135604000,
    "originalUserName": "Carmen ",
    "originalUserEmail": "onlylove_carmen@hotmail.com",
    "originalUserIp": "24.132.131.187",
    "status": null,
    "parents":null,
    "id": "f67822f6-e14c-44e6-a1f2-6355c93b796b",
    "heliosId": 3738571,
    "updatedDate": 1505145453955
},
{
    "user": null,
    "titles":null,
    "texts": null,
    "travelDate": 1223746391000,
    "locale": "nl",
    "ratings": {
        "general": {
            "general": 8
        },
        "aspects": {
            "location": 8,
            "service": 9,
            "priceQuality": 8,
            "food": 8,
            "room": 8,
            "childFriendly": 7,
            "interior": null,
            "size": null,
            "activities": null,
            "restaurants": null,
            "sanitaryState": null,
            "accessibility": null,
            "nightlife": null,
            "culture": null,
            "surrounding": null,
            "atmosphere": null,
            "noviceSkiArea": null,
            "advancedSkiArea": null,
            "apresSki": null,
            "beach": null,
            "entertainment": null,
            "environmental": null,
            "pool": null,
            "terrace": null,
            "housing": null,
            "hygiene": null
        }
    },
    "traveledWith": null,
    "cooperation": null,
    "entryDate": 1505145440909,
    "originalUserName": "aad schipper",
    "originalUserEmail": "aad@schipp.speedlinq.nl",
    "originalUserIp": "195.241.64.129",
    "status": null,
    "parents":null,
    "id": "d311bc2f-ee13-4a66-a0f7-8a03f26c5021",
    "heliosId": 3708594,
    "updatedDate": 1505145440909
}
]

describe('Scoring weight - over 5 years', () => {
  it('should return 0.5', () => {
    
    const result = Scoring.computeWeight(reviews[0]);
    
    expect(result).to.equal(0.5);
  });
});

describe('Scoring weight - 1 year difference', () => {
    it('should return 0.9', () => {
      
      const result = Scoring.computeWeight(reviews[1]);
      
      expect(result).to.equal(0.9);
    });
  });


describe('Scoring - general',()=>{
    it('should return 8.36',()=>{
        const scores = Scoring.compute(reviews);
        expect(scores.general.general).to.equal(8.36);
    });
});


describe('Scoring - aspects - not null',()=>{
    it('should return 8.36',()=>{
        const scores = Scoring.compute(reviews);
        expect(scores.aspects.location).to.equal(8.36);
    });
});

describe('Scoring - aspects - both null',()=>{
    it('should return 0',()=>{
        const scores = Scoring.compute(reviews);
        expect(scores.aspects.hygiene).to.equal(0);
    });
});

describe('Scoring - aspects - one null',()=>{
    it('should return 3,57',()=>{
        const scores = Scoring.compute(reviews);
        expect(scores.aspects.interior).to.equal(3.57);
    });
});