using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.TestHost;
using System;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;
using Shouldly;
using Shouldly.ShouldlyExtensionMethods;
using RestEase;
using Newtonsoft.Json;
using com.zoover.model;
using System.Collections.Generic;
using com.zoover.context;

namespace com.zoover.dataservice.tests
{

    public interface IDataServiceApi
    {
        [Get("/api/accomodations/")]
        Task<List<Accomodation>> GetAllAccomodation();

        [Get("/api/accomodations/{id}")]
        Task<Accomodation> GetSingleAccomodation([Path]Guid id);
    }

    public class DataService_IntegrationTests
    {
        [Fact]
        public async void DataService_GetAccomodationsAsync_Returns_All_Accomodations()
        {
            var guid = new Guid();
            var accomodation = new Accomodation
            {
                AccomodationType = AccomodationTypeEnum.Hotel,
                Address = new Address
                {
                    Street = "Bologna, Italy",
                    ZipCode = "40133"
                },
                Geo = new Geo
                {
                    Type = "POINT",
                    Latitude = 44.5048454,
                    Longitude = 11.309353
                },
                UpdatedDate = (long)DateTime.Now.Subtract(DateTime.MinValue.AddYears(1969)).TotalMilliseconds,
                ID = guid
            };

            _context.Accomodations.Add(accomodation);
            _context.SaveChanges();

            var result = await _usersApi.GetAllAccomodation();
            result.ShouldNotBeNull();

            result.Count.ShouldBe(1);

            result.ForEach(a => a.ShouldNotBeNull());
        }

        [Fact]
        public async void DataService_GetAccomodationAsync_Returns_Not_Null_Accomodation()
        {
            var guid = Guid.Parse("918f9459-487a-409b-8ef9-d1c6a7f3f29b");
            var street = "Via Emilia Ponente";
            var accomodation = new Accomodation
            {
                AccomodationType = AccomodationTypeEnum.Hotel,
                Address = new Address
                {
                    Street = street,
                    ZipCode = "40133"
                },
                Geo = new Geo
                {
                    Type = "POINT",
                    Latitude = 44.5048454,
                    Longitude = 11.309353
                },
                UpdatedDate = (long)DateTime.Now.Subtract(DateTime.MinValue.AddYears(1969)).TotalMilliseconds,
                ID = guid
            };

            _context.Accomodations.Add(accomodation);
            _context.SaveChanges();
            var result = await _usersApi.GetSingleAccomodation(guid);
            result.ShouldNotBeNull();
            result.Address.ShouldNotBeNull();
            result.Address.Street.ShouldNotBeNull();
            result.Address.Street.ShouldBe(street);
        }

        [Fact]
        public async void DataService_GetAccomodationAsync_Returns_Null_Accomodation()
        {
            Guid guid = Guid.Empty;
            //GET 404
            await _usersApi.GetSingleAccomodation(guid).ShouldThrowAsync<ApiException>();
            
        }

        private readonly IDataServiceApi _usersApi;
        private readonly TestServer _testServer;
        private Context _context;

        public DataService_IntegrationTests()
        {
            var webHostBuilder = new WebHostBuilder()
                                    .UseEnvironment("Testing")
                                    .UseStartup<Startup>();

            _testServer = new TestServer(webHostBuilder);
            //get in-memory context from server
            _context = _testServer.Host.Services.GetService(typeof(Context)) as Context;
            _usersApi = RestClient.For<IDataServiceApi>(_testServer.CreateClient());
        }
    
    }
}
