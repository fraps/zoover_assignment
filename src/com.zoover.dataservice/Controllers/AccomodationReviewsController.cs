﻿using com.zoover.context;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;
using System.Linq;

namespace com.zoover.dataservice.Controllers
{
    [Produces("application/json")]
    [Route("api/AccomodationReviews")]
    public class AccomodationReviewsController : Controller
    {
        private readonly Context _context;

        public AccomodationReviewsController(Context context)
        {
            _context = context;

        }

        // GET api/<controller>/00000000-0000-0000-0000-000000000000
        [HttpGet("{id}")]
        public async Task<IActionResult> GetAsync(Guid id)
        {
            //Retrieve all reviews related to accomodation
            var result = await _context.Reviews
                                .Include(r => r.Cooperation)
                                    .ThenInclude(c => c.SyndicationPartner)
                                .Include(r => r.Status)
                                .Include(r => r.Texts)
                                .Include(r => r.User)
                                .Include(r => r.Titles)
                                .Include(r => r.Ratings)
                                    .ThenInclude(r => r.General)
                                .Include(r => r.Ratings)
                                    .ThenInclude(r => r.Aspects)
                                .Include(r => r.Parents)
                                    .ThenInclude(p => p.Parent)
                                .Where(a => a.Parents.Any(p=>p.Parent.ID == id))
                                .ToListAsync();

            if (null == result) return NotFound();

            return Ok(result);
        }
    }
}
