C# Coding Challenge
============

Prerequisites
------------

Install [.Net Core SDK](https://www.microsoft.com/net/download) for Windows, MacOS or Linux

### Database connection ###

Change value of ZooverDB connection string in following files:

- ./src/com.zoover.context/appsettings.json
- ./src/com.zoover.jsonimporter/appsettings.json
- ./src/com.zoover.dataservice/appsettings.json

~~~json
{
  "ConnectionStrings": {
    "ZooverDB": "Server=localhost,1433\\dbo;Database=assignment;User id=SA;Password=Passw0rd;"
  }
}
~~~

Task 1 - Data Import Tool
------------

### Apply DB migrations ###

Initialize database running migrations
~~~powershell
    > cd .\src\com.zoover.context\
    > dotnet build
    > dotnet ef database update
~~~
### Run Importer ###
Build and publish importer from .\src.\com.zoover.jsonimporter 
~~~powershell
    > dotnet publish -c Release -o ..\..\dist\com.zoover.jsonimporter
~~~
And run it from root folder, passing json file and file type (Review/Accomodation)
~~~powershell
    > dotnet .\dist\com.zoover.jsonimporter\com.zoover.jsonimporter.dll -f accommodations.json -t Accomodation
    > dotnet .\dist\com.zoover.jsonimporter\com.zoover.jsonimporter.dll -f reviews.json -t Review
~~~

Task 2 - Data Service
------------

### Build and run Web Api project ###
Build and publish project from .\src\com.zoover.dataservice 
~~~powershell
    > dotnet publish -c Release -o ..\..\dist\com.zoover.dataservice
~~~

Start web service
~~~powershell
    > dotnet .\dist\com.zoover.dataservice\com.zoover.dataservice.dll
~~~

Service is set to run on localhost:5000

Integration tests can be run from .\src\com.zoover.dataservicetest 
~~~powershell
    > dotnet build
    > dotnet test
~~~

Task 3 - Scoring Service
------------

### Transpile and run Node.js project ###

Build Node.js project from ./src/com.zoover.scoring

Install dependencies
~~~bash
    $ npm install
~~~

Transpile typescript classes
~~~bash
    $ gulp scripts
~~~

Start server
~~~bash
    $ npm start
~~~

Service is set to run on localhost:3000

Optionally, run tests (requires Mocha and Chai)
~~~bash
    $ npm run test
~~~


Notes and considerations
------------

#### Entity Framework ####
As part of Data Access Layer, Entity Framework Core object-relational mapper is used, in order to simplify database design through automatic migrations and access to data. However, it also introduces overhead and loss of query optimization, which can result in performance degradation. 

#### Data Service ####
Web services required for Task 2 have been implemented with 2 web api controllers (one for accomodations, one for reviews), providing 2 endpoints each (all accomodations, single accomodation, all reviews, single review). Solution can yet been enhanced with pagination strategies to handle significantly bigger amounts of data to retrieve.
A third endpoint has been added to support retrieval of all reviews related to a single accomodation for Task 3. Same consideration on performances applies.

#### Scoring Service ####
Scoring service required for Task 3 has been implemented with a single endpoint getting accomodation ID as input, and querying wep api support endpoint to retrieve all related reviews, and applying a reduce function to compute the scores. In case pagination is needed, a small refactor to make it work with chunks of data should be applied.

API Documentation:
------------

###Get Accomodation (Task 2)###
----
  Returns json data about a single accomodation.

* **URL**

  /api/accomodations/:id

* **Method:**

  `GET`

*  **URL Params**

   **Required:**

   `id=[guid]`

* **Data Params**

  None

* **Success Response:**

  * **Code:** 200
  * **Content:** accomodation json

* **Error Response:**

  * **Code:** 404 NOT FOUND`

* **Sample Call:**
```shell
	curl -X "localhost:5000/api/accomodations/ddc0e073-004f-49ac-90c4-48ddbad0cb80"
```

###Get Accomodations (Task 2)###
----
  Returns json list of all accomodations.

* **URL**

  /api/accomodations/

* **Method:**

  `GET`

*  **URL Params**

  None

* **Data Params**

  None

* **Success Response:**

  * **Code:** 200
  * **Content:** accomodation list json


* **Sample Call:**
```shell
	curl -X "localhost:5000/api/accomodations"
```

###Get Review (Task 2)###
----
  Returns json data about a single review.

* **URL**

  /api/reviews/:id

* **Method:**

  `GET`

*  **URL Params**

   **Required:**

   `id=[guid]`

* **Data Params**

  None

* **Success Response:**

  * **Code:** 200
  * **Content:** review json

* **Error Response:**

  * **Code:** 404 NOT FOUND

* **Sample Call:**
```shell
	curl -X "localhost:5000/api/reviews/b3de3052-d672-4d16-8187-60adf342a30f"
```

###Get Reviews (Task 2)###
----
  Returns json list of all reviews.

* **URL**

  /api/reviews/

* **Method:**

  `GET`

*  **URL Params**

  None

* **Data Params**

  None

* **Success Response:**

  * **Code:** 200
  * **Content:** review list json

* **Sample Call:**
```shell
	curl -X "localhost:5000/api/reviews"
```


###Get Accomodation Reviews (helper API for Task 3)###
----
  Returns json list of reviews of a single accomodation.

* **URL**

  /api/accomodationReviews/:accomodationId

* **Method:**

  `GET`

*  **URL Params**

   **Required:**

   `accomodationId=[guid]`

* **Data Params**

  None

* **Success Response:**

  * **Code:** 200
  * **Content:** reviews json list

* **Error Response:**

  * **Code:** 404 NOT FOUND

* **Sample Call:**
```shell
	curl -X "localhost:5000/api/accomodationReviews/ddc0e073-004f-49ac-90c4-48ddbad0cb80"
```

###Scoring (Task 3)###
----
  Returns scores computed on reviews for an accomodation.

* **URL**

  /api/scoring/:accomodationId

* **Method:**

  `GET`

*  **URL Params**

   **Required:**

   `accomodationId=[guid]`

* **Data Params**

  None

* **Success Response:**

  * **Code:** 200
  * **Content:** 
~~~
    {
        "scores": {
            "general": {
                "general": 8.99
            },
            "aspects": {
                "location": 9.46,
                "service": 8.76,
                "priceQuality": 8.65,
                "food": 2.16,
                "room": 2.3,
                "childFriendly": 1.01,
                "interior": 0,
                "size": 0,
                "activities": 0,
                "restaurants": 0,
                "sanitaryState": 0,
                "accessibility": 0,
                "nightlife": 0,
                "culture": 0,
                "surrounding": 0,
                "atmosphere": 0,
                "noviceSkiArea": 0,
                "advancedSkiArea": 0,
                "apresSki": 0,
                "beach": 0,
                "entertainment": 0,
                "environmental": 0,
                "pool": 0,
                "terrace": 0,
                "housing": 0,
                "hygiene": 0
            }
        }
    }
~~~

* **Sample Call:**
```shell
	curl -X "localhost:5000/api/scoring/ddc0e073-004f-49ac-90c4-48ddbad0cb80"
```

