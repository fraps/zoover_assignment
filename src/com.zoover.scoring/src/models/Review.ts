export class User{
    id:string;
}

export class Titles
{
    nl:string;
}

export class Texts
{
    nl:string;
}

export class GeneralRatings
{
    general : number;
}

export class AspectRatings
{
    location: number;
    service: number;
    priceQuality: number;
    food: number;
    room: number;
    childFriendly: number;
    interior: number;
    size: number;
    activities: number;
    restaurants: number;
    sanitaryState: number;
    accessibility: number;
    nightlife: number;
    culture: number;
    surrounding: number;
    atmosphere: number;
    noviceSkiArea: number;
    advancedSkiArea: number;
    apresSki: number;
    beach: number;
    entertainment: number;
    environmental: number;
    pool: number;
    terrace: number;
    housing: number;
    hygiene: number
}

export class Ratings
{
    general : GeneralRatings;
    aspects : AspectRatings;
}

export enum TraveledWithEnum
{
    FAMILY,
    COUPLE,
    FRIENDS,
    OTHER,
    SINGLE,
    BUSINESS,
    FAMILY_WITH_YOUNG_CHILDREN,
    FAMILY_WITH_OLDER_CHILDREN,
}

export class SyndicationPartner
{
    id : string;
}

export class Cooperation
{
    importPartner : string;
    syndicationPartner: SyndicationPartner;
}

export class Status
{
    published : boolean;
    checked : boolean;
    reason : string;
}

export class Parent
{
    id : string;
}


export class Review
{
    user : User;
    titles : Titles;
    texts : Texts;
    travelDate : number;
    locale : string;
    ratings : Ratings;
    traveledWith : TraveledWithEnum;
    entryDate : number;
    originalUserName : string;
    originalUserEmail : string;
    originalUserIp : string;
    parents : Parent[];
    id:string;
    heliosId : number;
    updatedDate : number;
    cooperation: Cooperation;
    status: Status;

}

