﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace com.zoover.model
{

    public class User
    {
        public Guid ID { get; set; }
    }

    public class Titles
    {
        [JsonIgnore]
        public Guid ID { get; set; }

        public string NL { get; set; }
    }

    public class Texts
    {
        [JsonIgnore]
        public Guid ID { get; set; }

        public string NL { get; set; }
    }

    public class GeneralRating
    {
        [JsonIgnore]
        public Guid ID { get; set; }

        public int? General { get; set; }
    }

    public class AspectsRating
    {
        [JsonIgnore]
        public Guid ID { get; set; }

        public int? Location { get; set; }
        public int? Service { get; set; }
        public int? PriceQuality { get; set; }
        public int? Food { get; set; }
        public int? Room { get; set; }
        public int? ChildFriendly { get; set; }
        public int? Interior { get; set; }
        public int? Size { get; set; }
        public int? Activities { get; set; }
        public int? Restaurants { get; set; }
        public int? SanitaryState { get; set; }
        public int? Accessibility { get; set; }
        public int? Nightlife { get; set; }
        public int? Culture { get; set; }
        public int? Surrounding { get; set; }
        public int? Atmosphere { get; set; }
        public int? NoviceSkiArea { get; set; }
        public int? AdvancedSkiArea { get; set; }
        public int? ApresSki { get; set; }
        public int? Beach { get; set; }
        public int? Entertainment { get; set; }
        public int? Environmental { get; set; }
        public int? Pool { get; set; }
        public int? Terrace { get; set; }
        public int? Housing { get; set; }
        public int? Hygiene { get; set; }
    }

    public class Ratings
    {
        [JsonIgnore]
        public Guid ID { get; set; }

        public GeneralRating General { get; set; }

        public AspectsRating Aspects { get; set; }
    }

    public enum TraveledWithEnum
    {
        FAMILY,
        COUPLE,
        FRIENDS,
        OTHER,
        SINGLE,
        BUSINESS,
        FAMILY_WITH_YOUNG_CHILDREN,
        FAMILY_WITH_OLDER_CHILDREN,
    }

    public class SyndicationPartner
    {
        public Guid ID { get; set; }
    }

    public class Cooperation
    {
        [JsonIgnore]
        public Guid ID { get; set; }

        public string ImportPartner { get; set; }

        public SyndicationPartner SyndicationPartner { get; set; }
    }

    public class ReviewParent // Assumed to actually be related accomodation - correct solution would be linking to Accomodation object (FK to accomodations table)
    {
        [JsonIgnore]
        public Guid ReviewParentID { get; set; }

        [JsonIgnore]
        public Parent Parent { get; set; }

        [NotMapped]
        public Guid ID { get => Parent.ID; set => Parent = new Parent { ID = value }; }
    }

    public class Review
    {
        public User User { get; set; }

        public Titles Titles { get; set; }

        public Texts Texts { get; set; }

        public long TravelDate { get; set; }

        public string Locale { get; set; }

        public Ratings Ratings { get; set; }

        public TraveledWithEnum TraveledWith { get; set; }

        public Cooperation Cooperation { get; set; }

        public long EntryDate { get; set; }

        public string OriginalUserName { get; set; }

        public string OriginalUserEmail { get; set; }
        
        public string OriginalUserIP { get; set; }

        public Status Status { get; set; }

        public List<ReviewParent> Parents { get; set; } 

        public Guid ID { get; set; }

        public long HeliosID { get; set; }

        public long UpdatedDate { get; set; }
    }
}
