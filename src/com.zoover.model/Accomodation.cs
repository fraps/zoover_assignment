﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Linq;
using System.ComponentModel.DataAnnotations.Schema;

namespace com.zoover.model
{
    public enum AccomodationTypeEnum
    {
        Hotel,
        Appartement,
        Aparthotel,
    }

    public class Address
    {
        [JsonIgnore] // ID needed for PK in DB
        public Guid ID { get; set; }

        public string Street { get; set; }

        public string ZipCode { get; set; }
    }

    public class MainName
    {
        [JsonIgnore]
        public Guid ID { get; set; }

        public string NL { get; set; }
    }


    public class Names
    {
        [JsonIgnore]
        public Guid ID { get; set; }

        public MainName Main { get; set; } //Or List<MainName> or Dictionary<string, string> if other translations available

        public string FallBack { get; set; }
    }

    public class ContactInformation
    {
        [JsonIgnore]
        public Guid ID { get; set; }

        public string Phone { get; set; }

        public string Email { get; set; }

        public string Url { get; set; }
    }

    public class HistoricalUrl
    {
        [JsonIgnore]
        public Guid ID { get; set; }

        public string OldUrl { get; set; }

        public string NewUrl { get; set; }
    }
    
    public class Status
    {
        [JsonIgnore]
        public Guid ID { get; set; }

        public bool Published { get; set; }

        public bool Checked { get; set; }

        public string Reason { get; set; } // OR ENUM?
    }

    public class Geo
    {
        [JsonIgnore]
        public Guid ID { get; set; }


        // Flatten Coordinates array to properties to map to DB
        [NotMapped]
        public double[] Coordinates { get => new double[] { Longitude, Latitude }; set { Latitude = value[1]; Longitude = value[0]; } }

        [JsonIgnore]
        public double Latitude { get; set; }

        [JsonIgnore]
        public double Longitude { get; set; }

        public string Type { get; set; } // OR ENUM
    }

    public class Media
    {
        public Guid ID { get; set; }
    }

    public class AwardYear
    {
        [JsonIgnore]
        public Guid ID { get; set; }

        public string Year { get; set; }
    }
    

    public class Awards
    {
        [JsonIgnore]
        public Guid ID { get; set; }

        // Remap list of strings to single string mapped in db
        [JsonIgnore]
        public string WinnerYearsList { get => string.Join("|",WinnerYears ?? new List<string>()); set { WinnerYears = value.Split('|').ToList(); } }

        [NotMapped]
        public List<string> WinnerYears { get; set; }

        // Remap list of strings to single string mapped in db
        [JsonIgnore]
        public string RecommendedYearsList { get => string.Join("|", RecommendedYears ?? new List<string>()); set { RecommendedYears = value.Split('|').ToList(); } }

        [NotMapped]
        public List<string> RecommendedYears { get; set; }
        
    }

    public class AccomodationFacility
    {
        [JsonIgnore]
        public Guid AccomodationFacilityID { get; set; }
        
        // Need an object to explicitate navigation property for N:M relationship
        [JsonIgnore]
        public Facility Facility { get; set; }

        [NotMapped]
        public Guid ID { get => Facility.ID; set => Facility = new Facility { ID = value }; }
    }

    public class Facility
    {
        public Guid ID { get; set; }
    }

    public class Owner
    {
        public Guid ID { get; set; }
    }

    public class Parent
    {      

        public Guid ID { get; set; }
    }

    public class AccomodationParent
    {
        [JsonIgnore]
        public Guid AccomodationParentID { get; set; }

        // Need an object to explicitate navigation property for N:M relationship
        [JsonIgnore]
        public Parent Parent { get; set; }

        [NotMapped]
        public Guid ID { get => Parent.ID; set { Parent = new Parent { ID = value }; } }
    }

    public class Accomodation
    {
        public int VrwID { get; set; }

        public Names Names { get;set; }

        public List<AccomodationParent> Parents { get; set; }

        public AccomodationTypeEnum AccomodationType { get; set; }

        public Media Media { get; set; }

        public Geo Geo { get; set; }

        public int Stars { get; set; }

        public Address Address { get; set; }

        public ContactInformation ContactInformation { get; set; }
        
        public Awards Awards { get; set; }

        public List<AccomodationFacility> Facilities { get; set; }

        public string HeliosUrl { get; set; }

        public List<HistoricalUrl> HeliosHistoricalUrls { get; set; }

        public bool BookingComBlockEnabled { get; set; }

        public bool PremiumAdlinkEnabled { get; set; }

        public Owner Owner { get; set; }

        public Status Status { get; set; }


        public Guid ID { get; set; }

        public int HeliosID { get; set; }

        public decimal PopularityScore { get; set; }

        public long UpdatedDate { get; set; }
    }
}
