﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using com.zoover.context;
using com.zoover.model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace com.zoover.dataservice.Controllers
{
    [Route("api/[controller]")]
    public class AccomodationsController : Controller
    {

        private readonly Context _context;

        public AccomodationsController(Context context)
        {
            _context = context;

        }

        // GET: api/<controller>
        [HttpGet]
        public async Task<IActionResult> GetAsync() {
            var result= await _context.Accomodations
                                        .Include(a => a.Media)
                                        .Include(a => a.Names)
                                            .ThenInclude(n => n.Main)
                                        .Include(a => a.Geo)
                                        .Include(a => a.Address)
                                        .Include(a => a.ContactInformation)
                                        .Include(a => a.Awards)
                                        .Include(a => a.Facilities)
                                            .ThenInclude(f => f.Facility)
                                        .Include(a => a.Owner)
                                        .Include(a => a.Status)
                                        .Include(a => a.HeliosHistoricalUrls)
                                        .Include(a => a.Parents)
                                            .ThenInclude(p => p.Parent)
                                        .ToListAsync();

            if (null == result) return NotFound();

            return Ok(result);
        }

        // GET api/<controller>/00000000-0000-0000-0000-000000000000
        [HttpGet("{id}")]
        public async Task<IActionResult> GetAsync(Guid id)
        {
            var result = await _context.Accomodations
                                        .Include(a => a.Media)
                                        .Include(a => a.Names)
                                            .ThenInclude(n => n.Main)
                                        .Include(a => a.Geo)
                                        .Include(a => a.Address)
                                        .Include(a => a.ContactInformation)
                                        .Include(a => a.Awards)
                                        .Include(a => a.Facilities)
                                            .ThenInclude(f => f.Facility)
                                        .Include(a => a.Owner)
                                        .Include(a => a.Status)
                                        .Include(a => a.HeliosHistoricalUrls)
                                        .Include(a => a.Parents)
                                            .ThenInclude(p => p.Parent)
                                        .FirstOrDefaultAsync(a => a.ID == id);

            if (null == result) return NotFound();

            return Ok(result);
        }
    }
}
