﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace com.zoover.context.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "addresses",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    Street = table.Column<string>(nullable: true),
                    ZipCode = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_addresses", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "AspectsRating",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    Accessibility = table.Column<int>(nullable: true),
                    Activities = table.Column<int>(nullable: true),
                    AdvancedSkiArea = table.Column<int>(nullable: true),
                    ApresSki = table.Column<int>(nullable: true),
                    Atmosphere = table.Column<int>(nullable: true),
                    Beach = table.Column<int>(nullable: true),
                    ChildFriendly = table.Column<int>(nullable: true),
                    Culture = table.Column<int>(nullable: true),
                    Entertainment = table.Column<int>(nullable: true),
                    Environmental = table.Column<int>(nullable: true),
                    Food = table.Column<int>(nullable: true),
                    Housing = table.Column<int>(nullable: true),
                    Hygiene = table.Column<int>(nullable: true),
                    Interior = table.Column<int>(nullable: true),
                    Location = table.Column<int>(nullable: true),
                    Nightlife = table.Column<int>(nullable: true),
                    NoviceSkiArea = table.Column<int>(nullable: true),
                    Pool = table.Column<int>(nullable: true),
                    PriceQuality = table.Column<int>(nullable: true),
                    Restaurants = table.Column<int>(nullable: true),
                    Room = table.Column<int>(nullable: true),
                    SanitaryState = table.Column<int>(nullable: true),
                    Service = table.Column<int>(nullable: true),
                    Size = table.Column<int>(nullable: true),
                    Surrounding = table.Column<int>(nullable: true),
                    Terrace = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspectsRating", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "awards",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    RecommendedYearsList = table.Column<string>(nullable: true),
                    WinnerYearsList = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_awards", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "awards_years",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    Year = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_awards_years", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "contact_information",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    Email = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: true),
                    Url = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_contact_information", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "facilities",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_facilities", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "GeneralRating",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    General = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GeneralRating", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "geo",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    Latitude = table.Column<double>(nullable: false),
                    Longitude = table.Column<double>(nullable: false),
                    Type = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_geo", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "main_names",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    NL = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_main_names", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "media",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_media", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "owners",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_owners", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "parents",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_parents", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "statuses",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    Checked = table.Column<bool>(nullable: false),
                    Published = table.Column<bool>(nullable: false),
                    Reason = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_statuses", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "SyndicationPartner",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SyndicationPartner", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "texts",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    NL = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_texts", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "titles",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    NL = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_titles", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "users",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_users", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "ratings",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    AspectsID = table.Column<Guid>(nullable: true),
                    GeneralID = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ratings", x => x.ID);
                    table.ForeignKey(
                        name: "FK_ratings_AspectsRating_AspectsID",
                        column: x => x.AspectsID,
                        principalTable: "AspectsRating",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ratings_GeneralRating_GeneralID",
                        column: x => x.GeneralID,
                        principalTable: "GeneralRating",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "names",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    FallBack = table.Column<string>(nullable: true),
                    MainID = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_names", x => x.ID);
                    table.ForeignKey(
                        name: "FK_names_main_names_MainID",
                        column: x => x.MainID,
                        principalTable: "main_names",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "cooperations",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    ImportPartner = table.Column<string>(nullable: true),
                    SyndicationPartnerID = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_cooperations", x => x.ID);
                    table.ForeignKey(
                        name: "FK_cooperations_SyndicationPartner_SyndicationPartnerID",
                        column: x => x.SyndicationPartnerID,
                        principalTable: "SyndicationPartner",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "accomodations",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    AccomodationType = table.Column<int>(nullable: false),
                    AddressID = table.Column<Guid>(nullable: true),
                    AwardsID = table.Column<Guid>(nullable: true),
                    BookingComBlockEnabled = table.Column<bool>(nullable: false),
                    ContactInformationID = table.Column<Guid>(nullable: true),
                    GeoID = table.Column<Guid>(nullable: true),
                    HeliosID = table.Column<int>(nullable: false),
                    HeliosUrl = table.Column<string>(nullable: true),
                    MediaID = table.Column<Guid>(nullable: true),
                    NamesID = table.Column<Guid>(nullable: true),
                    OwnerID = table.Column<Guid>(nullable: true),
                    PopularityScore = table.Column<decimal>(nullable: false),
                    PremiumAdlinkEnabled = table.Column<bool>(nullable: false),
                    Stars = table.Column<int>(nullable: false),
                    StatusID = table.Column<Guid>(nullable: true),
                    UpdatedDate = table.Column<long>(nullable: false),
                    VrwID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_accomodations", x => x.ID);
                    table.ForeignKey(
                        name: "FK_accomodations_addresses_AddressID",
                        column: x => x.AddressID,
                        principalTable: "addresses",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_accomodations_awards_AwardsID",
                        column: x => x.AwardsID,
                        principalTable: "awards",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_accomodations_contact_information_ContactInformationID",
                        column: x => x.ContactInformationID,
                        principalTable: "contact_information",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_accomodations_geo_GeoID",
                        column: x => x.GeoID,
                        principalTable: "geo",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_accomodations_media_MediaID",
                        column: x => x.MediaID,
                        principalTable: "media",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_accomodations_names_NamesID",
                        column: x => x.NamesID,
                        principalTable: "names",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_accomodations_owners_OwnerID",
                        column: x => x.OwnerID,
                        principalTable: "owners",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_accomodations_statuses_StatusID",
                        column: x => x.StatusID,
                        principalTable: "statuses",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "reviews",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    CooperationID = table.Column<Guid>(nullable: true),
                    EntryDate = table.Column<long>(nullable: false),
                    HeliosID = table.Column<long>(nullable: false),
                    Locale = table.Column<string>(nullable: true),
                    OriginalUserEmail = table.Column<string>(nullable: true),
                    OriginalUserIP = table.Column<string>(nullable: true),
                    OriginalUserName = table.Column<string>(nullable: true),
                    RatingsID = table.Column<Guid>(nullable: true),
                    StatusID = table.Column<Guid>(nullable: true),
                    TextsID = table.Column<Guid>(nullable: true),
                    TitlesID = table.Column<Guid>(nullable: true),
                    TravelDate = table.Column<long>(nullable: false),
                    TraveledWith = table.Column<int>(nullable: false),
                    UpdatedDate = table.Column<long>(nullable: false),
                    UserID = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_reviews", x => x.ID);
                    table.ForeignKey(
                        name: "FK_reviews_cooperations_CooperationID",
                        column: x => x.CooperationID,
                        principalTable: "cooperations",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_reviews_ratings_RatingsID",
                        column: x => x.RatingsID,
                        principalTable: "ratings",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_reviews_statuses_StatusID",
                        column: x => x.StatusID,
                        principalTable: "statuses",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_reviews_texts_TextsID",
                        column: x => x.TextsID,
                        principalTable: "texts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_reviews_titles_TitlesID",
                        column: x => x.TitlesID,
                        principalTable: "titles",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_reviews_users_UserID",
                        column: x => x.UserID,
                        principalTable: "users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "accomodation_facilities",
                columns: table => new
                {
                    AccomodationFacilityID = table.Column<Guid>(nullable: false),
                    AccomodationID = table.Column<Guid>(nullable: true),
                    FacilityID = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_accomodation_facilities", x => x.AccomodationFacilityID);
                    table.ForeignKey(
                        name: "FK_accomodation_facilities_accomodations_AccomodationID",
                        column: x => x.AccomodationID,
                        principalTable: "accomodations",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_accomodation_facilities_facilities_FacilityID",
                        column: x => x.FacilityID,
                        principalTable: "facilities",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "accomodation_parents",
                columns: table => new
                {
                    AccomodationParentID = table.Column<Guid>(nullable: false),
                    AccomodationID = table.Column<Guid>(nullable: true),
                    ParentID = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_accomodation_parents", x => x.AccomodationParentID);
                    table.ForeignKey(
                        name: "FK_accomodation_parents_accomodations_AccomodationID",
                        column: x => x.AccomodationID,
                        principalTable: "accomodations",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_accomodation_parents_parents_ParentID",
                        column: x => x.ParentID,
                        principalTable: "parents",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "historical_urls",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    AccomodationID = table.Column<Guid>(nullable: true),
                    NewUrl = table.Column<string>(nullable: true),
                    OldUrl = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_historical_urls", x => x.ID);
                    table.ForeignKey(
                        name: "FK_historical_urls_accomodations_AccomodationID",
                        column: x => x.AccomodationID,
                        principalTable: "accomodations",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "review_parents",
                columns: table => new
                {
                    ReviewParentID = table.Column<Guid>(nullable: false),
                    ParentID = table.Column<Guid>(nullable: true),
                    ReviewID = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_review_parents", x => x.ReviewParentID);
                    table.ForeignKey(
                        name: "FK_review_parents_parents_ParentID",
                        column: x => x.ParentID,
                        principalTable: "parents",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_review_parents_reviews_ReviewID",
                        column: x => x.ReviewID,
                        principalTable: "reviews",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_accomodation_facilities_AccomodationID",
                table: "accomodation_facilities",
                column: "AccomodationID");

            migrationBuilder.CreateIndex(
                name: "IX_accomodation_facilities_FacilityID",
                table: "accomodation_facilities",
                column: "FacilityID");

            migrationBuilder.CreateIndex(
                name: "IX_accomodation_parents_AccomodationID",
                table: "accomodation_parents",
                column: "AccomodationID");

            migrationBuilder.CreateIndex(
                name: "IX_accomodation_parents_ParentID",
                table: "accomodation_parents",
                column: "ParentID");

            migrationBuilder.CreateIndex(
                name: "IX_accomodations_AddressID",
                table: "accomodations",
                column: "AddressID");

            migrationBuilder.CreateIndex(
                name: "IX_accomodations_AwardsID",
                table: "accomodations",
                column: "AwardsID");

            migrationBuilder.CreateIndex(
                name: "IX_accomodations_ContactInformationID",
                table: "accomodations",
                column: "ContactInformationID");

            migrationBuilder.CreateIndex(
                name: "IX_accomodations_GeoID",
                table: "accomodations",
                column: "GeoID");

            migrationBuilder.CreateIndex(
                name: "IX_accomodations_MediaID",
                table: "accomodations",
                column: "MediaID");

            migrationBuilder.CreateIndex(
                name: "IX_accomodations_NamesID",
                table: "accomodations",
                column: "NamesID");

            migrationBuilder.CreateIndex(
                name: "IX_accomodations_OwnerID",
                table: "accomodations",
                column: "OwnerID");

            migrationBuilder.CreateIndex(
                name: "IX_accomodations_StatusID",
                table: "accomodations",
                column: "StatusID");

            migrationBuilder.CreateIndex(
                name: "IX_cooperations_SyndicationPartnerID",
                table: "cooperations",
                column: "SyndicationPartnerID");

            migrationBuilder.CreateIndex(
                name: "IX_historical_urls_AccomodationID",
                table: "historical_urls",
                column: "AccomodationID");

            migrationBuilder.CreateIndex(
                name: "IX_names_MainID",
                table: "names",
                column: "MainID");

            migrationBuilder.CreateIndex(
                name: "IX_ratings_AspectsID",
                table: "ratings",
                column: "AspectsID");

            migrationBuilder.CreateIndex(
                name: "IX_ratings_GeneralID",
                table: "ratings",
                column: "GeneralID");

            migrationBuilder.CreateIndex(
                name: "IX_review_parents_ParentID",
                table: "review_parents",
                column: "ParentID");

            migrationBuilder.CreateIndex(
                name: "IX_review_parents_ReviewID",
                table: "review_parents",
                column: "ReviewID");

            migrationBuilder.CreateIndex(
                name: "IX_reviews_CooperationID",
                table: "reviews",
                column: "CooperationID");

            migrationBuilder.CreateIndex(
                name: "IX_reviews_RatingsID",
                table: "reviews",
                column: "RatingsID");

            migrationBuilder.CreateIndex(
                name: "IX_reviews_StatusID",
                table: "reviews",
                column: "StatusID");

            migrationBuilder.CreateIndex(
                name: "IX_reviews_TextsID",
                table: "reviews",
                column: "TextsID");

            migrationBuilder.CreateIndex(
                name: "IX_reviews_TitlesID",
                table: "reviews",
                column: "TitlesID");

            migrationBuilder.CreateIndex(
                name: "IX_reviews_UserID",
                table: "reviews",
                column: "UserID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "accomodation_facilities");

            migrationBuilder.DropTable(
                name: "accomodation_parents");

            migrationBuilder.DropTable(
                name: "awards_years");

            migrationBuilder.DropTable(
                name: "historical_urls");

            migrationBuilder.DropTable(
                name: "review_parents");

            migrationBuilder.DropTable(
                name: "facilities");

            migrationBuilder.DropTable(
                name: "accomodations");

            migrationBuilder.DropTable(
                name: "parents");

            migrationBuilder.DropTable(
                name: "reviews");

            migrationBuilder.DropTable(
                name: "addresses");

            migrationBuilder.DropTable(
                name: "awards");

            migrationBuilder.DropTable(
                name: "contact_information");

            migrationBuilder.DropTable(
                name: "geo");

            migrationBuilder.DropTable(
                name: "media");

            migrationBuilder.DropTable(
                name: "names");

            migrationBuilder.DropTable(
                name: "owners");

            migrationBuilder.DropTable(
                name: "cooperations");

            migrationBuilder.DropTable(
                name: "ratings");

            migrationBuilder.DropTable(
                name: "statuses");

            migrationBuilder.DropTable(
                name: "texts");

            migrationBuilder.DropTable(
                name: "titles");

            migrationBuilder.DropTable(
                name: "users");

            migrationBuilder.DropTable(
                name: "main_names");

            migrationBuilder.DropTable(
                name: "SyndicationPartner");

            migrationBuilder.DropTable(
                name: "AspectsRating");

            migrationBuilder.DropTable(
                name: "GeneralRating");
        }
    }
}
