﻿using com.zoover.model;
using System;
using Microsoft.EntityFrameworkCore;

namespace com.zoover.context
{
    public class Context : DbContext
    {

        public Context(DbContextOptions<Context> options)
       : base(options)
        { 
            this.ChangeTracker.AutoDetectChangesEnabled = false;
        }

        public DbSet<Accomodation> Accomodations { get; set; }
        public DbSet<Review> Reviews { get; set; }

      
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //ACCOMODATIONS
            modelBuilder.Entity<Accomodation>().ToTable("accomodations");
            modelBuilder.Entity<Media>().ToTable("media");
            modelBuilder.Entity<Owner>().ToTable("owners");
            modelBuilder.Entity<Awards>().ToTable("awards");
            modelBuilder.Entity<AwardYear>().ToTable("awards_years");
            modelBuilder.Entity<Geo>().ToTable("geo");
            modelBuilder.Entity<Address>().ToTable("addresses");
            modelBuilder.Entity<HistoricalUrl>().ToTable("historical_urls");
            modelBuilder.Entity<MainName>().ToTable("main_names");
            modelBuilder.Entity<Names>().ToTable("names");
            modelBuilder.Entity<Status>().ToTable("statuses");
            modelBuilder.Entity<ContactInformation>().ToTable("contact_information");
            modelBuilder.Entity<Parent>().ToTable("parents");
            modelBuilder.Entity<AccomodationParent>().ToTable("accomodation_parents").HasKey(r => r.AccomodationParentID);
            modelBuilder.Entity<AccomodationFacility>().ToTable("accomodation_facilities").HasKey(f => f.AccomodationFacilityID);
            modelBuilder.Entity<Facility>().ToTable("facilities");

            //ACCOMODATIONS - Navigation properties
            modelBuilder.Entity<Accomodation>().HasMany(a => a.Parents);
            modelBuilder.Entity<AccomodationParent>().HasOne(a => a.Parent);
            modelBuilder.Entity<Accomodation>().HasMany(a => a.Facilities);
            modelBuilder.Entity<AccomodationFacility>().HasOne(a => a.Facility);
            modelBuilder.Entity<Accomodation>().HasMany(a => a.HeliosHistoricalUrls);
            modelBuilder.Entity<Accomodation>().HasOne(a => a.Awards);
            
            //REVIEWS
            modelBuilder.Entity<Review>().ToTable("reviews");
            modelBuilder.Entity<Ratings>().ToTable("ratings");
            modelBuilder.Entity<Cooperation>().ToTable("cooperations");
            modelBuilder.Entity<User>().ToTable("users");
            modelBuilder.Entity<Titles>().ToTable("titles");
            modelBuilder.Entity<Texts>().ToTable("texts");
            modelBuilder.Entity<ReviewParent>().ToTable("review_parents").HasKey(r=>r.ReviewParentID);

            //REVIEWS - Navigation Properties
            modelBuilder.Entity<Review>().HasOne(r => r.User);
            modelBuilder.Entity<Review>().HasMany (r => r.Parents);
            modelBuilder.Entity<ReviewParent>().HasOne(r => r.Parent);
        }


    }
}
