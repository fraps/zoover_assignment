import { Review, Ratings, GeneralRatings } from "./models/Review";
import * as moment from 'moment';

export class Scores {
    ratings: Ratings;
    weight: number;
}


export class Scoring {

    static computeWeight(review : Review) : number {
        //let yearsDifference = moment().diff(review.entryDate,'years',false);
        let yearsDifference = moment().year() - moment(review.entryDate).year();
        return yearsDifference >= 5 ? 0.5 : 1 - yearsDifference*0.1;
    }

    static compute(reviews: Review[]): Ratings {
       

        let result = reviews.reduce<Scores>((partial: Scores, review: Review, index:number, array:Review[]) => {
            
            var weight = Scoring.computeWeight(review);

            partial.ratings.general.general += review.ratings.general.general*weight;
            
            Object.keys(partial.ratings.aspects).forEach(
                (aspect) => partial.ratings.aspects[aspect]+=review.ratings.aspects[aspect] * weight
              );

            partial.weight += weight;
            return partial;
        },
        {
            ratings: {
                general: {
                    general: 0
                },
                aspects: {
                    location: 0,
                    service: 0,
                    priceQuality: 0,
                    food: 0,
                    room: 0,
                    childFriendly: 0,
                    interior: 0,
                    size: 0,
                    activities: 0,
                    restaurants: 0,
                    sanitaryState: 0,
                    accessibility: 0,
                    nightlife: 0,
                    culture: 0,
                    surrounding: 0,
                    atmosphere: 0,
                    noviceSkiArea: 0,
                    advancedSkiArea: 0,
                    apresSki: 0,
                    beach: 0,
                    entertainment: 0,
                    environmental: 0,
                    pool: 0,
                    terrace: 0,
                    housing: 0,
                    hygiene: 0
                }
            },
            weight:0
        });

        const applyWeightAndTruncateNumber = (total,weight, decimals)=>Number((total / weight).toFixed(decimals));

        result.ratings.general.general=applyWeightAndTruncateNumber(result.ratings.general.general,result.weight,2);
        Object.keys(result.ratings.aspects).forEach(
            (aspect) => result.ratings.aspects[aspect]=applyWeightAndTruncateNumber(result.ratings.aspects[aspect], result.weight,2)
          );
        return result.ratings;
    }
}