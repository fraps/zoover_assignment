import * as path from 'path';
import * as express from 'express';
import * as logger from 'morgan';
import * as bodyParser from 'body-parser';
import * as http from 'http';
import { Review, Ratings } from './models/Review';
import { Scoring, Scores } from './Scoring';

// Creates and configures an ExpressJS web server.
class App {

    // ref to Express instance
    public express: express.Application;

    //Run configuration methods on the Express instance.
    constructor() {
        this.express = express();
        this.middleware();
        this.routes();
    }

    // Configure Express middleware.
    private middleware(): void {
        this.express.use(logger('dev'));
        this.express.use(bodyParser.json());
        this.express.use(bodyParser.urlencoded({ extended: false }));
    }

    // Configure API endpoints.
    private routes(): void {
        /* This is just to get up and running, and to make sure what we've got is
         * working so far. This function will change when we start to add more
         * API endpoints */
        let router = express.Router();
        // placeholder route handler
        router.get('/api/scoring/:id', (req, res, next) => {
            let accomodationId = req.params.id;
            http.get({
                host: 'localhost',
                port: '5000',
                path: '/api/accomodationReviews/'+accomodationId
            }, function (response) {
                // Continuously update stream with data
                var body = '';
                response.on('data', function (d) {
                    body += d;
                });
                response.on('end', function () {

                    // Data reception is done, do whatever with it!
                    let parsed: Review[];
                    let result: Ratings;
                    parsed = JSON.parse(body);

                    result = Scoring.compute(parsed);

                    res.json({
                        scores: result
                    });
                });
            });

        });
        this.express.use('/', router);
    }

}

export default new App().express;